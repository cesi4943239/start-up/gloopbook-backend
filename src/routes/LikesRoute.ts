import express, { Router, Request, Response, NextFunction } from "express"
import LikeController from "../controllers/LikeController.js"

class LikesRoute {
    public router: Router
    private likeController: LikeController = new LikeController()

    constructor() {
        this.router = express.Router()
        this.initializeRoutes()
    }

    initializeRoutes(): void {
        this.router.get(
            "/:id",
            this.getById),
        this.router.get(
            "/",
            this.getAll)
        this.router.post(
            "/",
            this.create
        )
    }

    getById = (req: Request, res: Response, next: NextFunction): void => {
        // this.postController.getById(req, res, next)
    }
    getAll = (req: Request, res: Response, next: NextFunction): void => {
        this.likeController.getAll(req, res, next)
    }
    create = (req: Request, res: Response, next: NextFunction): void => {
        this.likeController.create(req, res, next)
    }
}

export default LikesRoute 
