import express, { Router, Request, Response, NextFunction } from "express"
import UsersController from "../controllers/userController.js"

class UsersRoute {
    public router: Router
    private usersController: UsersController = new UsersController()

    constructor() {
        this.router = express.Router()
        this.initializeRoutes()
    }

    initializeRoutes(): void {
        this.router.get(
            "/:id",
            this.getById),
        this.router.get(
            "/",
            this.getAll),
        this.router.post(
            "/",
            this.create)
    }

    getById = (req: Request, res: Response, next: NextFunction): void => {
        this.usersController.getById(req, res, next)
    }
    getAll = (req: Request, res: Response, next: NextFunction): void => {
        this.usersController.getAll(req, res, next)
    }
    create = (req: Request, res: Response, next: NextFunction): void => {
        this.usersController.create(req, res, next)
    }
    
}

export default UsersRoute 
