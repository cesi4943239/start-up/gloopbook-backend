import express, { Router, Request, Response, NextFunction } from "express"
import UsersController from "../controllers/userController.js"
import PostController from "../controllers/PostController.js"

class PostsRoute {
    public router: Router
    private postController: PostController = new PostController()

    constructor() {
        this.router = express.Router()
        this.initializeRoutes()
    }

    initializeRoutes(): void {
        this.router.get(
            "/:id",
            this.getById),
        this.router.get(
            "/",
            this.getAll),
        this.router.post(
            "/",
            this.create),
        this.router.delete(
            "/:id",
            this.delete)
    }

    getById = (req: Request, res: Response, next: NextFunction): void => {
        this.postController.getById(req, res, next)
    }
    getAll = (req: Request, res: Response, next: NextFunction): void => {
        this.postController.getAll(req, res, next)
    }
    create = (req: Request, res: Response, next: NextFunction): void => {
        this.postController.create(req, res, next)
    }
    delete = (req: Request, res: Response, next: NextFunction): void => {
        this.postController.delete(req, res, next)
    }
}

export default PostsRoute 
