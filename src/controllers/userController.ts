import { Request, Response, NextFunction } from "express"
import UserRepository from '../repositories/UsersRepository.js'
import User from "../schemas/UsersSchemas.js"

class UsersController {
    private userRepository: UserRepository = new UserRepository()
    
    constructor() {}

    async getById(req: Request, res: Response, next: NextFunction): Promise<void> {
        try{
            // const id: number = parseInt(req.params.id)
            // const user = this.usersBusiness.getById(id)

            // if(user) { res.status(201).json({ data: user }) }

        } catch(err: any) {
            next(err)
        }
    }

    async getAll(req: Request, res: Response, next: NextFunction): Promise<void> {
        try{
            const user = await this.userRepository.getAll()
            if(user) { res.status(201).json({ data: user }) }
        } catch(err: any) {
            next(err)
        }
    }

    async create(req: Request, res: Response, next: NextFunction): Promise<void> {
        try{
            const { name, email, password } = req.body
            const user = new User({ name, email, password })
            const savedUser = await user.save()
            if (savedUser) {
                res.status(201).json({ data: savedUser })
            } else {
                res.status(500).json({ message: 'Erreur lors de la création du user' })
            }
        } catch(err: any) {
            next(err)
        }
    }

    async delete(req: Request, res: Response, next: NextFunction): Promise<void> {
        try{
            const id: any = req.params.id
            const user: any = await User.findByIdAndDelete(id)

            if(user) { res.status(201).json({ data: user }) }
        } catch(err: any) {
            next(err)
        }
    }
}

export default UsersController