import { Request, Response, NextFunction } from "express"
import LikesRepository from "../repositories/LikesRepository.js"
import Like from "../schemas/LikesSchemas.js"

class LikesController {
    private likeRepository: LikesRepository = new LikesRepository()
    
    constructor() {}

    async getById(req: Request, res: Response, next: NextFunction): Promise<void> {
        try{
            // const id: number = parseInt(req.params.id)
            // const user = this.usersBusiness.getById(id)

            // if(user) { res.status(201).json({ data: user }) }

        } catch(err: any) {
            next(err)
        }
    }

    async getAll(req: Request, res: Response, next: NextFunction): Promise<void> {
        try{
            console.log('LikesController - getAll')
            // const posts = await this.likeRepository.getAll()
            const likes = await Like.find({})
            if(likes) { res.status(201).json({ data: likes }) }
        } catch(err: any) {
            next(err)
        }
    }

    async create(req: Request, res: Response, next: NextFunction): Promise<void> {
        try{
            console.log('LikesController - create')
            const { postId, userId } = req.body;
            console.log(postId)
            console.log(userId)
            const like = new Like({ postId, userId });
            const savedLike = await like.save();
            if (savedLike) {
                res.status(201).json({ data: savedLike });
            } else {
                res.status(500).json({ message: 'Erreur lors de la création du like' });
            }
        } catch(err: any) {
            next(err)
        }
    }
}

export default LikesController