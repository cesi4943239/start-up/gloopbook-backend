import { Request, Response, NextFunction } from "express"
import PostsRepository from "../repositories/PostsRepository.js"
import Post from "../schemas/PostSchemas.js"
import Like from "../schemas/LikesSchemas.js"
import User from "../schemas/UsersSchemas.js"

class PostController {
    private postRepository: PostsRepository = new PostsRepository()
    
    constructor() {}

    async getById(req: Request, res: Response, next: NextFunction): Promise<void> {
        try{
            const id: any = req.params.id
            const post: any = await Post.findById(id)

            if(post) { res.status(201).json({ data: post }) }
        } catch(err: any) {
            next(err)
        }
    }

    async getAll(req: Request, res: Response, next: NextFunction): Promise<void> {
        try{
            const posts: any = await Post.find({}).populate('likes');
            
            if(posts) { res.status(201).json({ data: posts }) }
        } catch(err: any) {
            next(err)
        }
    }

    async create(req: Request, res: Response, next: NextFunction): Promise<void> {
        try{
            const { title, message, image } = req.body
            const post = new Post({ title, message, image })
            const savedPost = await post.save()
            if (savedPost) {
                res.status(201).json({ data: savedPost })
            } else {
                res.status(500).json({ message: 'Erreur lors de la création du like' })
            }
        } catch(err: any) {
            next(err)
        }
    }

    async delete(req: Request, res: Response, next: NextFunction): Promise<void> {
        try{
            const id: any = req.params.id
            const post: any = await Post.findByIdAndDelete(id)

            if(post) { res.status(201).json({ data: post }) }
        } catch(err: any) {
            next(err)
        }
    }
}

export default PostController