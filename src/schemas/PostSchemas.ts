import mongoose from "mongoose";
const Schema = mongoose.Schema;

const PostSchema = new Schema({
    title: { type: String, required: true },
    message: { type: String, required: true },
    image: { type: String, required: true },
    likes: [{ type: Schema.Types.ObjectId, ref: 'Like' }]
  });

const Post = mongoose.model('Post', PostSchema)

export default Post