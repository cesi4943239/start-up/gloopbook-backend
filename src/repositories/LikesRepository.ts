import Like from "../schemas/LikesSchemas.js";
import Post from "../schemas/PostSchemas.js"

class LikesRepository {

    likeModel = Like

    constructor() {}

    public async getAll() {
        try {
          console.log('PostRepo - getAll')
          const likes = await Like.find({})
          return likes
        } catch(err: any) {
          throw err as Error
        }
    }

    public async create() {
      
    }
}

export default LikesRepository
