import User from "../schemas/UsersSchemas.js"

class UserRepository {

    userModel = User
    constructor() {}

    public async findById(id: number) {
        try {
            const newUser = await this.userModel.findById(id)
            return newUser
        } catch(err: any) {
            throw err as Error
        }
    }

    public async getAll() {
        try {
          const users = await this.userModel.find({})
          return users
        } catch(err: any) {
          throw err as Error
        }
      }
}

export default UserRepository
