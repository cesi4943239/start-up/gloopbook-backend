import Post from "../schemas/PostSchemas.js"

class PostRepository {

    postModel = Post

    constructor() {}

    public async getAll() {
        try {
          console.log('PostRepo - getAll')
          const posts = await Post.find({}).populate('likes');
          return posts
        } catch(err: any) {
          throw err as Error
        }
      }
}

export default PostRepository
