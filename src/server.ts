import express from 'express'
import bodyParser from 'body-parser'
import cors from 'cors'
import UsersRoute from './routes/UsersRoute.js'
import Database from '../db/Database.js'
import PostsRoute from './routes/PostsRoute.js'
import LikesRoute from './routes/LikesRoute.js'

const app = express()
app.set("trust proxy", true)
app.use(bodyParser.json())

app.use(cors({
    origin: '*',
    methods: ['GET', 'POST', 'PUT', 'DELETE'],
    allowedHeaders: ['Content-Type', 'Authorization', 'Origin', 'X-Requested-With', 'Content', 'Accept']
}))

Database.init()

const usersRoute: UsersRoute = new UsersRoute()
const postRoute: PostsRoute = new PostsRoute()
const likeRoute: LikesRoute = new LikesRoute()

app.use('/users', usersRoute.router)
app.use('/posts', postRoute.router)
app.use('/likes', likeRoute.router)

export { app }