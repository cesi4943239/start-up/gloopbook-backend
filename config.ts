import * as dotenv from 'dotenv'

if (process.env.NODE_ENV && process.env.NODE_ENV == 'production') {
    console.log('PROD')
    dotenv.config({ path: '.env.production' });
}
if (process.env.NODE_ENV && process.env.NODE_ENV == 'development') {
    console.log('DEV')
    dotenv.config({ path: '.env.development' });
}

const config = {
    jwt: {
        SECRET_KEY: process.env.JWT_SECRET_KEY || "***",
        DURATION: process.env.JWT_DURATION || "***"
    },
    api: {
        port: process.env.API_PORT ? Number(process.env.API_PORT) : 12344,
        host: process.env.API_HOST || "***"
    },
    db: {
        port: process.env.DB_PORT ? Number(process.env.DB_PORT) : 12345,
        name: process.env.DB_NAME || "***",
        user: process.env.DB_USER || "***",
        user_pwd: process.env.DB_PWD || "",
        host: process.env.DB_HOST || "***"
    },
    auth: {
        github: {
            secret: process.env.AUTH_SECRET || "***",
            client_id: process.env.AUTH_GITHUB_ID || "***",
            client_secret: process.env.AUTH_GITHUB_SECRET || "***"
        }
    }
}

export default config