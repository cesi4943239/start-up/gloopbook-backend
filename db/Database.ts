import mongoose from "mongoose"
import mockUsers from "../db/mock-datas/users.js"
import mockPosts from "../db/mock-datas/posts.js"
import User from "../src/schemas/UsersSchemas.js";
import Post from "../src/schemas/PostSchemas.js";
import Like from "../src/schemas/LikesSchemas.js";

class Database {
    public static init() {
        mongoose.connect("mongodb://root:example@mongo:27017")
        .then(() => {
            Database.seed()
            console.log('Connected to MongoDB')
        })
        .catch((error) => {
            console.error('Error connecting to MongoDB:', error)
        })
    }

    public static async seed() {
        // await User.deleteMany({})
        // await Post.deleteMany({})
        // await Like.deleteMany({})
        await User.create(mockUsers)
        await Post.create(mockPosts)
    }
}

export default Database