FROM node:20-alpine

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install --global nodemon

RUN npm install

COPY . .

EXPOSE 3005

CMD ["npm", "run", "start:dev"]
