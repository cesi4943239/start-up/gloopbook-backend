import { app } from "./src/server.js"
import config from "./config.js"

const port = config.api.port
console.log(port)

app.listen(port, () => {
    console.log(`server listening on qzdqzd ${port}`)
})